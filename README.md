Tor Research Portal Website
===========================

This repository contains the source code for the Tor Research Portal.
The portal is a static website generated with the [Hugo](https://gohugo.io/)
static site generator. It uses a [Hugo theme
port](https://github.com/irl/torproject-hugo) of the [Tor
Styleguide](https://styleguide.torproject.org/). In the event that you are
looking to add/modify a technical report, you will also need to have
bibtex2html in your path.

Build and serve on localhost
----------------------------

```
make server
```

Adding a technical report
-------------------------

To add a technical report, place the final PDF to be published in the
`static/techreports/` directory. Add an entry to the BibTeX file
`techreports.bib`. This change will be made when you next run `make
server` or you can perform only the update step by running the
`update-techreports-html` script.

Deploy your changes to production/staging
-----------------------------------------

Simply push to git on the HEAD or staging branches. Changes on the
HEAD branch will be published directly to the main website:

<https://research.torproject.org/>

The staging branch can be used to proposed Merge Requests which will
show up as a GitLab pages when built. The URL for the GitLab pages is:

<https://tpo.pages.torproject.org/tpo/web/research/>

The state of the CI pipeline can be found [in GitLab](https://gitlab.torproject.org/tpo/web/research/-/pipelines).
